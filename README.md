# Cocoa Powder Box Simulator

This simulation demonstrates the composition of a typical industrial cocoa powder box, which often contains an excessive amount of sugar. You can adjust the total grams of powder and the sugar ratio to see the corresponding amounts of sugar and cocoa in the box.
